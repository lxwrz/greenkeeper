﻿using Greenkeeper.ParameterNullCheck;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.ParameterNullCheck
{
    [TestFixture]
    public class ContextActionExecuteTests : CSharpContextActionExecuteTestBase<ParameterNullCheckContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "ParameterNullCheckExecution"; }
        }

        protected override ParameterNullCheckContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var settings = new ParameterNullCheckSettings
            {
                AddNotNullAttributeToParameter = false
            };
            return ParameterNullCheckContextAction.Create(dataProvider, settings);
        }

        [TestCase("ExecutionOnCtorWithOneParameter")]
        [TestCase("ExecutionOnCtorWithTwoParameters")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatements")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneArgExceptionExists")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneArgExceptionAtWrongPosition")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneWrongArgExceptionExists")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneOtherArgExceptionExists")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneWrongArgExceptionEqualExists")]
        [TestCase("ExecutionOnCtorWithValueTypeParameter")]
        [TestCase("ExecutionOnMethodWithOneParameter")]
        [TestCase("ExecutionOnCtorWithoutNullCheckInThisCtor")]
        [TestCase("ExecutionOnCtorWithTwoNullCheckInMultiThisCtor")]
        [TestCase("ExecutionOnCtorWithParameterArgExceptionAndOtherArgException")]
        [TestCase("ExecutionOnCtorWithNullOrEmptyCheckInMultiThisCtor")]
        [TestCase("ExecutionOnCtorWithReferenceEqualCheckInMultiThisCtor")]
        [TestCase("ExecutionOnCtorWithSelfpointingThis")]
        [TestCase("ExecutionOnCtorWithEndlessLoopingThis")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

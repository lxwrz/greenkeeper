﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Psi;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionMultiExecuteConditionalTypeTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterMultiExecutionConditionalType"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var typeComparer = GetConditionalTypeComparer();
            var conAccessRightsComparer = GetConditionalAccessRightsComparer();
            var conAlphaNumericComparer = GetConditionalAlphaNumericComparer();

            var comparer = new MultiDeclarationComparer(
                new Collection<IDeclarationComparer>
                    {
                        typeComparer,
                        conAccessRightsComparer,
                        conAlphaNumericComparer
                    });

            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        private static TypeDeclarationComparer GetConditionalTypeComparer()
        {
            var orderType = new List<ElementTypeName>
                {
                    ElementTypeName.Method,
                    ElementTypeName.Property,
                    ElementTypeName.Field,
                    ElementTypeName.Constructor
                };
            var typeComparer = new TypeDeclarationComparer(orderType);
            return typeComparer;
        }

        private static ConditionalDeclarationComparer GetConditionalAlphaNumericComparer()
        {
            var alphaNumericComparer = new AlphanumericDeclarationComparer();
            var typeConditionForAlphaNumeric =
                new TypeDeclarationCondition(new Collection<ElementTypeName> {ElementTypeName.Method, ElementTypeName.Field});
            var conAlphaNumericComparer = new ConditionalDeclarationComparer(alphaNumericComparer,
                                                                                    typeConditionForAlphaNumeric);
            return conAlphaNumericComparer;
        }

        private static ConditionalDeclarationComparer GetConditionalAccessRightsComparer()
        {
            var orderAccessRights = new List<AccessRights>
                {
                    AccessRights.PUBLIC,
                    AccessRights.INTERNAL,
                    AccessRights.PROTECTED,
                    AccessRights.PRIVATE
                };
            var accessRightsComparer = new AccessRightsDeclarationComparer(orderAccessRights);
            var typeConditionForAccessRights =
                new TypeDeclarationCondition(new Collection<ElementTypeName>
                    {
                        ElementTypeName.Property,
                        ElementTypeName.Constructor
                    });
            var conAccessRightsComparer = new ConditionalDeclarationComparer(accessRightsComparer,
                                                                                    typeConditionForAccessRights);
            return conAccessRightsComparer;
        }

        [TestCase("ExecutionOnUnsorted1")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

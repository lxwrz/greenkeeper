﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionMultiExecuteRegionAlphanumericTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterMultiExecutionRegionAlphanumeric"; }
        }

        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var regionDeclaration = new RegionDeclarationComparer();
            var alphanumericDeclaration = new AlphanumericDeclarationComparer();
            var comparer = new MultiDeclarationComparer(
                new Collection<IDeclarationComparer>
                    {
                        regionDeclaration,
                        alphanumericDeclaration
                    });
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter, new RegionKeeperExecutor());
        }

        [TestCase("ExecutionOnUnsortedHierarchyRegions")]
        [TestCase("ExecutionOnUnsortedFlatRegions")]
        [TestCase("ExecutionOnUnsortedEqualNamedFlatRegions")]
        [TestCase("ExecutionOnUnsortedEqualNamedHierarchyRegions")]
        [TestCase("ExecutionOnUnsortedMultiHierarchyRegions")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

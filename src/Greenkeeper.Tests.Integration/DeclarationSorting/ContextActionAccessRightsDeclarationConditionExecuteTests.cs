﻿using System.Collections.Generic;
using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Psi;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionAccessRightsDeclarationConditionExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAccessRightsConditionExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var accessRightsCondition = new AccessRightsDeclarationCondition(new List<AccessRights> { AccessRights.PUBLIC, AccessRights.PRIVATE });
            var alphanumericComparer = new AlphanumericDeclarationComparer();
            var comparer = new ConditionalDeclarationComparer(alphanumericComparer, accessRightsCondition);
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsortedMembers")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedBaseElementExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterBaseElementExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithDeclarationBaseElementComparer"; }
        }

        [TestCase("ExecutionOnUnsortedWithOneInterface")]
        [TestCase("ExecutionOnUnsortedWithOneBaseClass")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfaces1")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfaces2")]
        [TestCase("ExecutionOnUnsortedWithOneBaseClassAndOneInterface1")]
        [TestCase("ExecutionOnUnsortedWithOneBaseClassAndOneInterface2")]
        [TestCase("ExecutionOnUnsortedWithOverlappingInterfaces")]
        [TestCase("ExecutionOnUnsortedWithHierarchicInterfaces1")]
        [TestCase("ExecutionOnUnsortedWithHierarchicInterfaces2")]
        [TestCase("ExecutionOnUnsortedWithHierarchicInterfaces3")]
        [TestCase("ExecutionOnUnsortedWithOneAbstractBaseClass")]
        [TestCase("ExecutionOnUnsortedWithOverlappingInterfacesOrderSwitched")]
        [TestCase("ExecutionOnUnsortedWithOneInterfaceNotDeclared")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfacesNotDeclared")]
        [TestCase("ExecutionOnUnsortedWithTwoOverlappingInterfacesNotDeclared")]
        [TestCase("ExecutionOnUnsortedWithInterfaceIncludesMethodsAndProperties")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfacesWithSameName")]
        [TestCase("ExecutionOnUnsortedWithOverridingObject")]
        [TestCase("ExecutionOnUnsortedWithInterfaceIncludesOnlyProperties")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

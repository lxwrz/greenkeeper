﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedAlphanumericExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAlphanumericExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithAlphanumericComparer"; }
        }
        
        [TestCase("ExecutionOnUnsortedMethods1")]
        [TestCase("ExecutionOnUnsortedMethods2")]
        [TestCase("ExecutionOnUnsortedMethods3")]
        [TestCase("ExecutionOnUnsortedMembers")]
        [TestCase("ExecutionOnUnsortedProperties")]
        [TestCase("ExecutionOnUnsortedConstants")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

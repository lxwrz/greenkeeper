﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedAlphanumericCodeFormatterExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAlphanumericWithCodeFormatExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithAlphanumericComparerAndCodeFormatterExecution"; }
        }
        
        [TestCase("ExecutionOnUnsortedNotFormatted")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

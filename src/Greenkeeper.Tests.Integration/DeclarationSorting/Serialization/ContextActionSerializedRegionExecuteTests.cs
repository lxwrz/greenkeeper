﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedRegionExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterRegionExecution"; }
        }
        
        protected override string XmlFileName
        {
            get { return "SorterWithRegionComparerAndAdditionalExecution"; }
        }

        [TestCase("ExecutionOnUnsortedHierarchyRegions")]
        [TestCase("ExecutionOnUnsortedFlatRegions")]
        [TestCase("ExecutionOnUnsortedEqualNamedFlatRegions")]
        [TestCase("ExecutionOnUnsortedEqualNamedHierarchyRegions")]
        [TestCase("ExecutionOnUnsortedMultiHierarchyRegions")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }

    }
}

﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    public abstract class ContextActionSerializedExecuteTestBase : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        protected abstract string XmlFileName {get;}

        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var loader = new DeclarationSortingDefinitionFromFileLoader(ExtraPath, XmlFileName);
            var definition = loader.Load();
            return DeclarationSorterContextAction.Create(dataProvider, definition.DeclarationSorter, definition.AdditionalExecutor);
        }
    }
}

﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedMultiExecuteAccessRightsUsageTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterMultiExecutionAccessRightsUsage"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithMultiAccessRightsUsageComparer"; }
        }
      
        [TestCase("ExecutionOnUnsortedMethods1")]
        [TestCase("ExecutionOnUnsortedMethods2")]
        [TestCase("ExecutionOnUnsortedMethods3")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{     
    [TestFixture]
    public class ContextActionRegionExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterRegionExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var comparer = new RegionDeclarationComparer();
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter, new RegionKeeperExecutor());
        }

        [TestCase("ExecutionOnUnsortedHierarchyRegions")]
        [TestCase("ExecutionOnUnsortedFlatRegions")]
        [TestCase("ExecutionOnUnsortedEqualNamedFlatRegions")]
        [TestCase("ExecutionOnUnsortedEqualNamedHierarchyRegions")]
        [TestCase("ExecutionOnUnsortedMultiHierarchyRegions")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

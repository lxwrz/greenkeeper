﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionAlphanumericAvailabilityTests : CSharpContextActionAvailabilityTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAlphanumericAvailability"; }
        }

        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var comparer = new AlphanumericDeclarationComparer();
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }
        
        [TestCase("AvailableOnUnsortedMethods")]
        [TestCase("NotAvailableOnSortedMethods")]
        [TestCase("AvailableOnUnsortedMembers")]
        [TestCase("NotAvailableOnSortedMembers")]
        [TestCase("AvailableOnUnsortedProperties")]
        [TestCase("NotAvailableOnSortedProperties")]
        [TestCase("AvailableOnUnsortedMix")]
        [TestCase("NotAvailableOnSortedMix")]
        public void TestExecution(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}

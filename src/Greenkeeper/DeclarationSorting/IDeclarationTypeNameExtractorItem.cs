﻿using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public interface IDeclarationTypeNameExtractorItem
    {
        bool IsType(IDeclaration declaration);
        string GetTypeName(IDeclaration declaration);
    }
}

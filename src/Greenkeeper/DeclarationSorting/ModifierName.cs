﻿namespace Greenkeeper.DeclarationSorting
{
    public enum ModifierName
    {
        None,
        Abstract,
        Sealed,
        Virtual,
        Override,
        Static,
        Readonly,
        Extern,
        Unsafe,
        Volatile
    }
}

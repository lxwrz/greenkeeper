﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class OperatorTypeNameExtractorItem : IDeclarationTypeNameExtractorItem
    {
        public bool IsType(IDeclaration declaration)
        {
            return declaration is ISignOperatorDeclaration || declaration is IOperatorDeclaration;
        }

        public string GetTypeName(IDeclaration declaration)
        {
            return ElementTypeName.Operator.ToString();
        }
    }
}

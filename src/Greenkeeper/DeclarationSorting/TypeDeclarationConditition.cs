﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class TypeDeclarationCondition : IDeclarationCondition
    {
        private readonly ICollection<ElementTypeName> _ElementTypeNames;

        public TypeDeclarationCondition(ICollection<ElementTypeName> elementTypeNames)
        {
            if (elementTypeNames == null) throw new ArgumentNullException("elementTypeNames");
            _ElementTypeNames = elementTypeNames;
        }

        public bool CheckCondition(DeclarationOrderElement declarationOrderElement)
        {
            var name = GetName(declarationOrderElement);
            return IsValidElementTypeName(name);
        }

        private string GetName(DeclarationOrderElement declarationOrderElement)
        {
            var extractor = DeclarationTypeNameExtractor.Create(declarationOrderElement);
            return extractor.GetTypeName();
        }

        private bool IsValidElementTypeName(string name)
        {
            return _ElementTypeNames.Any(e => String.Equals(e.ToString(),name,StringComparison.OrdinalIgnoreCase));
        }
    }
}

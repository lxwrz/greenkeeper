﻿using System;
using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    class ClassDeclarationReplacerProvider : ITreeNodeProvider
    {
        private readonly IClassDeclaration _ClassDeclaration;
        private readonly ICollection<IDeclarationReplaceItem> _DeclarationReplaceItems;

        public ClassDeclarationReplacerProvider(IClassDeclaration classDeclaration, ICollection<IDeclarationReplaceItem> declarationReplaceItems)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            if (declarationReplaceItems == null) throw new ArgumentNullException("declarationReplaceItems");
            _ClassDeclaration = classDeclaration;
            _DeclarationReplaceItems = declarationReplaceItems;
        }

        public IList<ITreeNode> GetTreeNodes()
        {
            var extractor = new ClassDeclarationExtractor(_ClassDeclaration);
            var declarations = extractor.GetDeclarations();
            return ReplaceDeclarations(declarations);
        }

        private IList<ITreeNode> ReplaceDeclarations(IList<IDeclaration> declarations)
        {
            var treeNodes = new List<ITreeNode>();
            foreach (var declaration in declarations)
            {
                treeNodes.Add(GetReplacedDeclaration(declaration));
            }
            return treeNodes;
        }

        private ITreeNode GetReplacedDeclaration(IDeclaration declaration)
        {
            var replacer = new DeclarationReplacer(_DeclarationReplaceItems);
            return replacer.GetReplacedDeclaration(declaration);
        }
    }
}

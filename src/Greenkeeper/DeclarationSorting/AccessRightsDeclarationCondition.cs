﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using JetBrains.ReSharper.Psi;

namespace Greenkeeper.DeclarationSorting
{
    public class AccessRightsDeclarationCondition : IDeclarationCondition
    {
        private readonly IList<AccessRights> _AccessRights;

        public AccessRightsDeclarationCondition([NotNull] IList<AccessRights> accessRights)
        {
            if (accessRights == null) throw new ArgumentNullException("accessRights");
            _AccessRights = accessRights;
        }

        public bool CheckCondition([NotNull] DeclarationOrderElement declarationOrderElement)
        {
            if (declarationOrderElement == null) throw new ArgumentNullException("declarationOrderElement");
            var declaration = declarationOrderElement.Declaration;
            var accessRightsOwner = declaration as IAccessRightsOwner;
            if (accessRightsOwner == null)
            {
                return false;
            }
            var accessRights = accessRightsOwner.GetAccessRights();
            return _AccessRights.Any(a => a.Equals(accessRights));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class BrokenRegionDetector
    {
        private readonly ICollection<RegionDeclarationRange> _PreviousRegionDeclarationRanges;
        private readonly ICollection<RegionDeclarationRange> _CurrentRegionDeclarationRanges;
        private RegionDeclarationRange _PreviousRegion;
        private RegionDeclarationRange _CurrentRegion;

        public BrokenRegionDetector(ICollection<RegionDeclarationRange> previousRegionDeclarationRanges, ICollection<RegionDeclarationRange> currentRegionDeclarationRanges)
        {
            if (previousRegionDeclarationRanges == null) throw new ArgumentNullException("previousRegionDeclarationRanges");
            if (currentRegionDeclarationRanges == null) throw new ArgumentNullException("currentRegionDeclarationRanges");

            _PreviousRegionDeclarationRanges = previousRegionDeclarationRanges;
            _CurrentRegionDeclarationRanges = currentRegionDeclarationRanges;
        }
       
        public ICollection<RegionDeclarationRange> Detect()
        {
            var brokenRegions = new Collection<RegionDeclarationRange>();
            foreach (var region in _CurrentRegionDeclarationRanges)
            {
                _CurrentRegion = region;
                if (!HasCurrentRegionAnyEqualDeclarationsInPreviousListing())
                {
                    brokenRegions.Add(_CurrentRegion);
                }
            }
            return brokenRegions;
        }

        private bool HasCurrentRegionAnyEqualDeclarationsInPreviousListing()
        {
            var previousRegions = FindCurrentRegionByNameInPreviousListing();
            bool anyHasEqualDeclarations = false;
            foreach (var previousRegion in previousRegions)
            {
                _PreviousRegion = previousRegion;
                if (HasPreviousAndCurrentEqualDeclarations())
                {
                    anyHasEqualDeclarations = true;
                }
            }
            return anyHasEqualDeclarations;
        }

        private ICollection<RegionDeclarationRange> FindCurrentRegionByNameInPreviousListing()
        {
            return _PreviousRegionDeclarationRanges.Where(r => String.Equals(r.Region.Name, _CurrentRegion.Region.Name)).ToList();
        }

        private bool HasPreviousAndCurrentEqualDeclarations()
        {
            var comparer = new ScrambledSequenceDeclarationComparer(_PreviousRegion.ChildDeclarations,
                                                             _CurrentRegion.ChildDeclarations);

            return comparer.AreEqual();
            
        }
    }
}

﻿using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationUsage
    {
        public IDeclaredElement DeclaredElement { get; private set; }
        public IDeclaration UsedBy { get; private set; }
        public TreeTextRange AtPosition { get; private set; }

        public DeclarationUsage(IDeclaredElement declaredElement, IDeclaration usedBy, TreeTextRange atPosition)
        {
            if (declaredElement == null) throw new ArgumentNullException("declaredElement");
            if (usedBy == null) throw new ArgumentNullException("usedBy");

            DeclaredElement = declaredElement;
            UsedBy = usedBy;
            AtPosition = atPosition;
        }
    }
}

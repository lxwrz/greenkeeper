﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("RegionKeeperExecutor")]
    public class RegionKeeperExecutorProxy : AdditionalExecutorProxyBase
    {
        public override IAdditionalExecutor CreateAdditionalExecutor()
        {
            return new RegionKeeperExecutor();
        }
    }
}

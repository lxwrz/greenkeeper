﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("AlphanumericDeclarationComparer")]
    public class AlphanumericDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            return new AlphanumericDeclarationComparer();
        }
    }
}

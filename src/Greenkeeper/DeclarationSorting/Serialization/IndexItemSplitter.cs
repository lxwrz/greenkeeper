﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    public class IndexItemSplitter<T>
    {
        private readonly string _Splittable;
        private readonly IItemConverter<T> _Converter;
        private ICollection<string> _Splitted;
        private IList<IndexItem<T>> _IndexItems; 

        private readonly string[] _SplitSymbolsSameIndex = new[] { "|" };
        private int _CurrentIndex;

        public IndexItemSplitter(string splittable, IItemConverter<T> converter)
        {
            if (splittable == null) throw new ArgumentNullException("splittable");
            if (converter == null) throw new ArgumentNullException("converter");
            _Splittable = splittable;
            _Converter = converter;
        }

        public IList<IndexItem<T>> ExecuteSplitting()
        {
            if (String.IsNullOrEmpty(_Splittable))
            {
                return new Collection<IndexItem<T>>();    
            }
            SplitDifferentIndex();
            SplitSameIndex();
            return _IndexItems;
        }

        private void SplitDifferentIndex()
        {
            _Splitted = new StringSplitter(_Splittable).ExecuteSplitting();
        }

        private void SplitSameIndex()
        {
            _CurrentIndex = 0;
            _IndexItems = new Collection<IndexItem<T>>();
            foreach (var splittedItem in _Splitted)
            {
                AddToIndexItems(splittedItem);
                _CurrentIndex++;
            }
        }
        
        private void AddToIndexItems(string splittedItem)
        {
            var splitResult = new StringSplitter(splittedItem,_SplitSymbolsSameIndex).ExecuteSplitting();
            foreach (var value in splitResult)
            {
                var converted = ConvertToItem(value);
                _IndexItems.Add(new IndexItem<T>(converted, _CurrentIndex));
            }
        }

        private T ConvertToItem(string value)
        {
            return _Converter.Convert(value);
        }
    }
}

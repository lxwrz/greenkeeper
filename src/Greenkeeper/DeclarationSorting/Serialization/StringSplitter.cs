﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    public class StringSplitter
    {
        private readonly string _Splittable;
        private readonly string[] _SplitSymbols;
        private readonly string[] _DefaultSplitSymbols = new [] {",",";"};
        
        public StringSplitter(string splittable)
        {
            if (splittable == null) throw new ArgumentNullException("splittable");
            _Splittable = splittable;
            _SplitSymbols = _DefaultSplitSymbols;
        }

        public StringSplitter(string splittable, string[] splitSymbols)
        {
            if (splittable == null) throw new ArgumentNullException("splittable");
            if (splitSymbols == null) throw new ArgumentNullException("splitSymbols");
            _Splittable = splittable;
            _SplitSymbols = splitSymbols;
        }

        public IList<string> ExecuteSplitting()
        {
            if (String.IsNullOrEmpty(_Splittable))
            {
                return new Collection<string>();    
            }
            return SplitAndTrim(_Splittable,_SplitSymbols);
        }

        private IList<string> SplitAndTrim(string toSplit, string[] splitSymbols)
        {
            var splitted = toSplit.Split(splitSymbols, StringSplitOptions.RemoveEmptyEntries);
            return splitted.Select(r => r.Trim()).ToList();
        }
    }
}

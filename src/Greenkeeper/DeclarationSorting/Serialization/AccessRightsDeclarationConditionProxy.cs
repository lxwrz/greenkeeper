﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("AccessRightsDeclarationCondition")]
    public class AccessRightsDeclarationConditionProxy : DeclarationConditionProxyBase
    {
        [XmlAttribute("AccessRights")]
        public string AccessRights { get; set; }

        public override IDeclarationCondition CreateDeclarationCondition()
        {
            var splitter = new AccessRightsSplitter();
            var accessRights = splitter.SplitFlat(AccessRights);
            return new AccessRightsDeclarationCondition(accessRights);
        }
    }
}

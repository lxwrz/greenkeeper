﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("MultiAdditionalExecutor")]
    public class MultiAdditionalExecutorProxy : AdditionalExecutorProxyBase
    {
        [XmlElement("AdditionalExecutor")]
        public List<XmlSerializable<AdditionalExecutorProxyBase>> AdditionalExecutorProxies { get; set; }

        public override IAdditionalExecutor CreateAdditionalExecutor()
        {
            var additionalExecuters = AdditionalExecutorProxies.Select(p => p.Serialized.CreateAdditionalExecutor()).ToList();
            return new MultiAdditionalExecutor(additionalExecuters);
        }
    }
}

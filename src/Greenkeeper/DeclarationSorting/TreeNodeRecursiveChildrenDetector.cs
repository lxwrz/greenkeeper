﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public static class TreeNodeRecursiveChildrenDetector
    {
        public static ICollection<T> ChildrenRecursive<T>(this ITreeNode treeNode)
            where T : ITreeNode
        {
            return ChildrenRecursive(treeNode).OfType<T>().ToList();
        }

        public static ICollection<ITreeNode> ChildrenRecursive(this ITreeNode treeNode)
        {
            return ChildrenRecursive(treeNode, AlwaysTrue);
        }

        private static bool AlwaysTrue(ITreeNode treeNode)
        {
            return true;
        }

        private static ICollection<ITreeNode> ChildrenRecursive(ITreeNode treeNode, Predicate<ITreeNode> matchPredicate)
        {
            var allChildren = new List<ITreeNode>();
            foreach (var child in treeNode.Children())
            {
                if (matchPredicate(child))
                {
                    allChildren.Add(child);
                }
                allChildren.AddRange(ChildrenRecursive(child, matchPredicate));
            }
            return allChildren;
        }

        public static ICollection<T> ChildrenRecursiveInTextRange<T>(this ITreeNode treeNode, TreeTextRange textRange)
           where T : ITreeNode
        {
            return ChildrenRecursiveInTextRange(treeNode,textRange).OfType<T>().ToList();
        }

        public static ICollection<ITreeNode> ChildrenRecursiveInTextRange(this ITreeNode treeNode, TreeTextRange textRange)
        {
            return ChildrenRecursive(treeNode, t => IsTreeNodeInTextRange(t,textRange));
        }

        private static bool IsTreeNodeInTextRange(ITreeNode treeNode, TreeTextRange textRange)
        {
            var treeTextRange = treeNode.GetTreeTextRange();
            var treeStartOffset = treeTextRange.StartOffset.Offset;

            var rangeStartOffset = textRange.StartOffset.Offset;
            var rangeEndOffset = textRange.EndOffset.Offset;

            return treeStartOffset > rangeStartOffset && treeStartOffset < rangeEndOffset;
        }
    }
}

﻿using JetBrains.Application.Settings;
using JetBrains.DocumentModel;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSorterDaemonProcess : IDaemonStageProcess
    {
        private readonly Logger _Logger;
        private readonly IDaemonProcess _DaemonProcess;
        private readonly IContextBoundSettingsStore _Settings;
        private IDeclarationSorter _DeclarationSorter;
        private IAdditionalExecutor _AdditionalExecutor;

        public DeclarationSorterDaemonProcess(IDaemonProcess daemonProcess, IContextBoundSettingsStore settings)
        {
            _DaemonProcess = daemonProcess;
            _Settings = settings;
            _Logger = LogManager.GetCurrentClassLogger();
        }

        public IDaemonProcess DaemonProcess
        {
          get { return _DaemonProcess; }
        }

        private string GetDaemonLogDescription()
        {
            return "DeclarationSorterDaemonProcessExecute on " + _DaemonProcess.SourceFile.Name;
        }

        public void Execute(Action<DaemonStageResult> commiter)
        {
            ExceptionLogger.Execute(_Logger, GetDaemonLogDescription, () => CommitDaemonStageResultAndLogTrackedTime(commiter));
        }

        private void CommitDaemonStageResultAndLogTrackedTime(Action<DaemonStageResult> commiter)
        {
            TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetDaemonLogDescription, () => CommitDaemonStageResult(commiter));
        }

        private void CommitDaemonStageResult(Action<DaemonStageResult> commiter)
        {
            if (_DaemonProcess.SourceFile.LanguageType is HtmlProjectFileType)
            {
                return;
            }

            IFile file = PsiFileFinder.FindCSharpPsiFile(_DaemonProcess);
            if (file == null)
            {
                return;
            }
           
            commiter(GetDaemonStageResult(file));
        }
        
        private DaemonStageResult GetDaemonStageResult(IFile file)
        {
            return new DaemonStageResult(GetHighlightingInfos(file));
        }

        private ICollection<HighlightingInfo> GetHighlightingInfos(IFile file)
        {
            SetSorterAndExecuter();
            var highlightings = new Collection<HighlightingInfo>();
            var classDeclarations = GetClassDeclarations(file);
            foreach (var classDeclaration in classDeclarations)
            {
                if (NeedHighlighting(classDeclaration))
                {
                    highlightings.Add(GetHighlightingInfos(classDeclaration));
                }
            }
            return highlightings;
        }

        private void SetSorterAndExecuter()
        {
            var loader = new DeclarationSortingDefinitionFromSettingsLoader(_Settings);
            var definition = loader.Load();
            _AdditionalExecutor = definition.AdditionalExecutor;
            _DeclarationSorter = definition.DeclarationSorter;
        }
        
        private ICollection<IClassDeclaration> GetClassDeclarations(IFile file)
        {
            if (file == null) throw new ArgumentNullException("file");
            return file.EnumerateSubTree().OfType<IClassDeclaration>().ToList();
        }

        private bool NeedHighlighting(IClassDeclaration classDeclaration)
        {
            _DeclarationSorter.InitWithClassDeclaration(classDeclaration);
            return !_DeclarationSorter.IsClassSorted();
        }

        private HighlightingInfo GetHighlightingInfos(IClassDeclaration classDeclaration)
        {
            return new HighlightingInfo(GetHighlightingDocumentRange(classDeclaration),
                                        new DeclarationSorterHighlighting(classDeclaration, _DeclarationSorter, _AdditionalExecutor));
        }
      
        private DocumentRange GetHighlightingDocumentRange(IClassDeclaration classDeclaration)
        {
            var rangeDetector = new ClassDeclarationRangeDetector(classDeclaration);
            return rangeDetector.GetHeaderRange();
        }
    }
}
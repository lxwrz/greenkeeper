﻿using System;
using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class RegionRemover
    {
        private readonly TreeNodeWriter _TreeNodeWriter;
        private readonly ICollection<IStartRegion> _StartRegions;

        public RegionRemover(TreeNodeWriter treeNodeWriter, ICollection<IStartRegion> startRegions)
        {
            if (treeNodeWriter == null) throw new ArgumentNullException("treeNodeWriter");
            if (startRegions == null) throw new ArgumentNullException("startRegions");
            _TreeNodeWriter = treeNodeWriter;
            _StartRegions = startRegions;
        }

        public void Remove()
        {
            foreach (var startRegion in _StartRegions)
            {
                RemoveRegion(startRegion);
            }
        }

        private void RemoveRegion(IStartRegion startRegion)
        {
            _TreeNodeWriter.RemoveTreeNode(startRegion.EndRegion);
            _TreeNodeWriter.RemoveTreeNode(startRegion);
        }
    }
}

﻿namespace Greenkeeper.DeclarationSorting
{
    public interface IDeclarationCondition
    {
        bool CheckCondition(DeclarationOrderElement declarationOrderElement);
    }
}

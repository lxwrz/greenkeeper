﻿namespace Greenkeeper.DeclarationSorting
{
    public class SortedIndexDeclarationComparer : DefaultDeclarationComparer
    {
        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            return GetCompareIndex(x).CompareTo(GetCompareIndex(y));
        }

        private int GetCompareIndex(DeclarationOrderElement declarationOrderElement)
        {
            if (!declarationOrderElement.SortedIndex.HasValue)
            {
                return int.MaxValue;
            }
            return declarationOrderElement.SortedIndex.Value;
        }
    }
}

﻿using System;
using Greenkeeper.DeclarationSorting.Serialization;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSorterSettingsChecker
    {
        private string _ErrorMessage = string.Empty;

        public bool IsSerializedDefinitionValid(DeclarationSorterSettings declarationSorterSettings)
        {
            var xmlDefinition = declarationSorterSettings.SerializedDeclarationSorterDefinition;
            try
            {
                var definition = DeserializeFromXmlDefinition(xmlDefinition);
                return IsValidDeclarationSortingDefinition(definition);
            }
            catch(Exception ex)
            {
                AppendErrorMessage(ex);
                return false;
            }
        }

        private DeclarationSortingDefinition DeserializeFromXmlDefinition(string xmlDefinition)
        {
            var serializer = new DeclarationSortingDefinitionSerializer(xmlDefinition);
            var proxy = serializer.Deserialize();
            return proxy.Create();
        }

        private bool IsValidDeclarationSortingDefinition(DeclarationSortingDefinition definition)
        {
            return definition != null && definition.DeclarationSorter != null && definition.AdditionalExecutor != null;
        }

        private void AppendErrorMessage(Exception ex)
        {
            _ErrorMessage += ex.Message + Environment.NewLine;
            if (ex.InnerException != null)
            {
                AppendErrorMessage(ex.InnerException);
            }
        }

        public string GetErrorMessage()
        {
            return _ErrorMessage;
        }
    }
}

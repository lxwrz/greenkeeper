﻿using System;
using JetBrains.DocumentModel;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Tree;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class RegionDeclarationRangeDetector
    {
        private readonly IFile _File;
        private ICollection<IStartRegion> _Regions;
        private ICollection<RegionDeclarationRange> _RegionDeclarationRanges; 

        public static RegionDeclarationRangeDetector CreateRegionDeclarationRangeDetector(ITreeNode treeNode)
        {
            var file = GetFile(treeNode);
            var detector = new RegionDeclarationRangeDetector(file);
            return detector;
        }

        private static IFile GetFile(ITreeNode node)
        {
            var sourceFile = node.GetSourceFile();
            if (sourceFile == null) throw new NullReferenceException("sourceFile");
            return PsiFileFinder.FindCSharpPsiFile(sourceFile);
        }
        
        public RegionDeclarationRangeDetector(IFile file)
        {
            _File = file;
        }

        public ICollection<RegionDeclarationRange> Detect()
        {
            FindRegions();
            CreateRegionDeclarationRanges();
            DetectChildDeclarations();
            return _RegionDeclarationRanges;
        }

        
        private void FindRegions()
        {
            _Regions = _File.ChildrenRecursive<IStartRegion>().ToList();
        }

        private void CreateRegionDeclarationRanges()
        {
            _RegionDeclarationRanges = new Collection<RegionDeclarationRange>();

            foreach (var startRegion in _Regions)
            {
                AddRegionDeclarationRange(startRegion);
            }
        }
        
        private void AddRegionDeclarationRange(IStartRegion startRegion)
        {
            var treeTextRange = GetTreeTextRangeForRegion(startRegion);
            var regionDeclarationRange = new RegionDeclarationRange(startRegion, treeTextRange);
            SetDeclarationsInRegionDeclarationRange(regionDeclarationRange);
            SetRegionsInRegionDeclarationRange(regionDeclarationRange);
            _RegionDeclarationRanges.Add(regionDeclarationRange);
        }

        private void SetDeclarationsInRegionDeclarationRange(RegionDeclarationRange regionDeclarationRange)
        {
            var treeTextRange = regionDeclarationRange.TreeTextRange;
            var declarations = _File.ChildrenRecursiveInTextRange<IDeclaration>(treeTextRange).ToList();
            regionDeclarationRange.SetDeclarationsInTreeTextRange(declarations);
        }

        private void SetRegionsInRegionDeclarationRange(RegionDeclarationRange regionDeclarationRange)
        {
            var treeTextRange = regionDeclarationRange.TreeTextRange;
            var childRegions = _File.ChildrenRecursiveInTextRange<IStartRegion>(treeTextRange).ToList();
            regionDeclarationRange.SetRegionsInTreeTextRange(childRegions);
        }

        private TreeTextRange GetTreeTextRangeForRegion(IStartRegion startRegion)
        {
            var start = startRegion.GetTreeTextRange().StartOffset;
            var end = startRegion.EndRegion.GetTreeTextRange().EndOffset;
            return new TreeTextRange(start, end);
        }

        private void DetectChildDeclarations()
        {
            foreach (var regionDeclarationRange in _RegionDeclarationRanges)
            {
                foreach (var declaration in regionDeclarationRange.DeclarationsInTreeTextRange)
                {
                    if (!ContainsAnySubRegionDeclaration(regionDeclarationRange.RegionsInTreeTextRange, declaration))
                    {
                        regionDeclarationRange.ChildDeclarations.Add(declaration);
                    }
                }
            }
        }

        private bool ContainsAnySubRegionDeclaration(ICollection<IStartRegion> subregions, IDeclaration declaration)
        {
            foreach (var region in subregions)
            {
                if (ContainsRegionDeclaration(region, declaration))
                {
                    return true;
                }
            }
            return false;
        }

        private bool ContainsRegionDeclaration(IStartRegion subRegion, IDeclaration declaration)
        {
            var regionDeclarationRange = _RegionDeclarationRanges.Single(r => Equals(r.Region, subRegion));
            return regionDeclarationRange.DeclarationsInTreeTextRange.Any(d => Equals(d, declaration));
        }

    }
}


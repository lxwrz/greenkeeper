﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    class ClassDeclarationExtractor
    {
        private readonly IClassDeclaration _ClassDeclaration;
       
        public ClassDeclarationExtractor(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            _ClassDeclaration = classDeclaration;
        }

        public IList<IDeclaration> GetDeclarations()
        {
            var declarationRange = _ClassDeclaration.GetAllDeclarationsRange();
            return declarationRange.Declarations.ToList();
        }
    }
}

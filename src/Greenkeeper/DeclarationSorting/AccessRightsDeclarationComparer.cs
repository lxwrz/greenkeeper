﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi;

namespace Greenkeeper.DeclarationSorting
{
    public class AccessRightsDeclarationComparer : ReferenceDeclarationComparer<string>
    {
        public AccessRightsDeclarationComparer(IList<AccessRights> accessRightsOrder)
            : this(IndexItem<AccessRights>.CreateIndexItems(accessRightsOrder))
        {
        }

        public AccessRightsDeclarationComparer(IList<IndexItem<AccessRights>> accessRightsOrder)
            : base(GetIndexEnumAsStrings(accessRightsOrder))
        {
        }
        
        protected override ICollection<string> GetReferencesFrom(DeclarationOrderElement declarationOrderElement)
        {
            var declaration = declarationOrderElement.Declaration;
            var accessRightsOwner = declaration as IAccessRightsOwner;
            if (accessRightsOwner == null)
            {
                return new Collection<string>();
            }
            var accessRights = accessRightsOwner.GetAccessRights();
            return new Collection<string>{ accessRights.ToString() };
        }
    }
}

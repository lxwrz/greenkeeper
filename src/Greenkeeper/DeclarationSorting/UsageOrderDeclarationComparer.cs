﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    class UsageOrderDeclarationComparer : IDeclarationComparer
    {
        private readonly IDeclarationComparer _ComparerForTrailingSort;
        private ICollection<DeclarationOrderElement> _DeclarationOrderElements;
        private ICollection<DeclarationUsage> _DeclarationUsages;
        private ICollection<DeclarationOrderElement> _TrailingSortedDeclarationOrderElements;
        private IList<DeclarationOrderElement> _SortedDeclarationOrderElements;

        public static IDeclarationComparer GetDefaultDeclarationComparer()
        {
            return GetDefaultDeclarationComparer(new Collection<IDeclarationComparer>());
        }

        public static IDeclarationComparer GetDefaultDeclarationComparer(ICollection<IDeclarationComparer> additionalComparer)
        {
            return new MultiDeclarationComparer(
                new Collection<IDeclarationComparer>
                    {
                        new SortedIndexDeclarationComparer(),
                        new UsageOrderRootDeclarationComparer()
                    }
                    .Union(additionalComparer)
                    .ToList());
        }

        public UsageOrderDeclarationComparer()
            : this(GetDefaultDeclarationComparer())
        {
        }

        public UsageOrderDeclarationComparer(IDeclarationComparer comparerForTrailingSort)
        {
            if (comparerForTrailingSort == null) throw new ArgumentNullException("comparerForTrailingSort");
            _ComparerForTrailingSort = comparerForTrailingSort;
        }

        public void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> elements)
        {
            if (elements == null) throw new ArgumentNullException("elements");
            _DeclarationOrderElements = elements;

            FindDeclarationUsages();
            TrailingSortDeclarationOrderElement();
            SortDeclarationOrderElement();
        }

        public bool NeedResort()
        {
            return false;
        }

        public bool NeedSortedInitWithDeclarationOrderElements()
        {
            return true;
        }

        public int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            return GetIndex(x).CompareTo(GetIndex(y));
        }

        private void FindDeclarationUsages()
        {
            var declarations = _DeclarationOrderElements.Select(d => d.Declaration).ToList();
            var finder = new DeclarationUsageInClassFinder(declarations);
            _DeclarationUsages = finder.Find();
        }

        private void TrailingSortDeclarationOrderElement()
        {
            var sorter = new DeclarationOrderElementSorter(_ComparerForTrailingSort, _DeclarationOrderElements);
            _TrailingSortedDeclarationOrderElements = sorter.Sort();
        }

        private void SortDeclarationOrderElement()
        {
            var sorter = new UsageOrderDeclarationSorter(_TrailingSortedDeclarationOrderElements, _DeclarationUsages);
            _SortedDeclarationOrderElements = sorter.Sort();
        }

        private int GetIndex(DeclarationOrderElement declarationOrderElement)
        {
            return _SortedDeclarationOrderElements.IndexOf(declarationOrderElement);
        }
    }
}

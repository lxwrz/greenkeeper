﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.DeclarationSorting
{
    public class MultiDeclarationComparer : IDeclarationComparer
    {
        private readonly ICollection<IDeclarationComparer> _DeclarationComparers;
        private ICollection<IDeclarationComparer> _ActiveDeclarationComparers;
        private int _ResortCounter;

        public MultiDeclarationComparer(ICollection<IDeclarationComparer> declarationComparers)
        {
            if (declarationComparers == null) throw new ArgumentNullException("declarationComparers");
            _DeclarationComparers = declarationComparers;
        }

        public bool NeedResort()
        {
            var isResortNeeded = IsResortNeeded();
            if (isResortNeeded)
            {
                PrepareForNextSort();
            }
            else
            {
                Clean();
            }
            return isResortNeeded;
        }

        private bool IsResortNeeded()
        {
            return _ActiveDeclarationComparers.Count != _DeclarationComparers.Count;
        }

        private void PrepareForNextSort()
        {
            _ResortCounter++;
        }

        private void Clean()
        {
            _ResortCounter = 0;
        }

        public bool NeedSortedInitWithDeclarationOrderElements()
        {
            return false;
        }

        public void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> elements)
        {
            if (elements == null) throw new ArgumentNullException("elements");

            SetActiveDeclarationComparers();
            foreach (var declarationComparer in _ActiveDeclarationComparers)
            {
                declarationComparer.InitWithDeclarationOrderElement(elements);
            }
        }

        private void SetActiveDeclarationComparers()
        {
            int allIteratedComparerWithNeedSortedInit = 0;
            _ActiveDeclarationComparers = new Collection<IDeclarationComparer>(); 
            foreach (var declarationComparer in _DeclarationComparers)
            {
                if (declarationComparer.NeedSortedInitWithDeclarationOrderElements())
                {
                    if (StopAddActiveDeclaration(allIteratedComparerWithNeedSortedInit))
                    {
                        break;
                    }
                    allIteratedComparerWithNeedSortedInit++;
                }
                _ActiveDeclarationComparers.Add(declarationComparer);
            }
        }

        private bool StopAddActiveDeclaration(int allIteratedComparerWithNeedSortedInit)
        {
            return allIteratedComparerWithNeedSortedInit == _ResortCounter;
        }

        public int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");

            foreach (var declarationComparer in _ActiveDeclarationComparers)
            {
                var compareResult = declarationComparer.Compare(x, y);
                if (compareResult != 0)
                {
                    return compareResult;
                }
            }

            return 0;
        }
    }
}

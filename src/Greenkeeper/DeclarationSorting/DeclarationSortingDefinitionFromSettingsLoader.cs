﻿using System;
using Greenkeeper.DeclarationSorting.Serialization;
using JetBrains.Application.Settings;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSortingDefinitionFromSettingsLoader
    {
        private readonly IContextBoundSettingsStore _SettingsStore;
        private DeclarationSorterSettings _Settings;

        public DeclarationSortingDefinitionFromSettingsLoader(IContextBoundSettingsStore settingsStore)
        {
            if (settingsStore == null) throw new ArgumentNullException("settingsStore");
            _SettingsStore = settingsStore;
        }

        public DeclarationSortingDefinition Load()
        {
            _Settings = GetDeclarationSorterSettings();
            return DeserializeSettings();
        }

        private DeclarationSorterSettings GetDeclarationSorterSettings()
        {
            var loader = new DeclarationSorterSettingsLoader(_SettingsStore);
            return loader.Load();
        }

        private DeclarationSortingDefinition DeserializeSettings()
        {
            var serializer = new DeclarationSortingDefinitionSerializer(GetSerializedDeclarationSorterDefinition());
            var proxy = serializer.Deserialize();
            return proxy.Create();
        }
        
        private string GetSerializedDeclarationSorterDefinition()
        {
            if (_Settings.UseDeclarationSorterDefinition)
            {
                return _Settings.SerializedDeclarationSorterDefinition;
            }
            return _Settings.GetDefaultSerializedDeclarationSorterDefinition();
        }
    }
}

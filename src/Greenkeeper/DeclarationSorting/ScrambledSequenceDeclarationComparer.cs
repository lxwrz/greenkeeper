﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class ScrambledSequenceDeclarationComparer
    {
        private readonly ICollection<IDeclaration> _DeclarationsA;
        private readonly ICollection<IDeclaration> _DeclarationsB;
        private ICollection<IDeclaration> _DeclarationsACopy;
        private ICollection<IDeclaration> _DeclarationsBCopy;
        private IDeclaration _CurrentDeclarationA;
        private IDeclaration _CurrentDeclarationB;
        
        public ScrambledSequenceDeclarationComparer(ICollection<IDeclaration> declarationsA, ICollection<IDeclaration> declarationsB)
        {
            if (declarationsA == null) throw new ArgumentNullException("declarationsA");
            if (declarationsB == null) throw new ArgumentNullException("declarationsB");
            _DeclarationsA = declarationsA;
            _DeclarationsB = declarationsB;
        }

        public bool AreEqual()
        {
            CopyListings();
            CrossJoinAndRemoveIfEqual();
            return AllItemsInCopyListingsAreRemoved();
        }

        private void CopyListings()
        {
            _DeclarationsACopy = _DeclarationsA.ToList();
            _DeclarationsBCopy = _DeclarationsB.ToList();
        }

        private void CrossJoinAndRemoveIfEqual()
        {
            foreach (var declarationA in _DeclarationsA)
            {
                _CurrentDeclarationA = declarationA;
                RemoveIfOtherListingContainsEqualDeclaration();
            }
        }

        private void RemoveIfOtherListingContainsEqualDeclaration()
        {
            foreach (var declarationB in _DeclarationsB)
            {
                _CurrentDeclarationB = declarationB;
                if (AreCurrentDeclarationsEqual())
                {
                    RemoveCurrentDeclarationsFromCopyListings();
                    break;
                }
            }
        }
        
        private bool AreCurrentDeclarationsEqual()
        {
            return DeclarationEqualityComparer.DeclarationComparer.Equals(_CurrentDeclarationA, _CurrentDeclarationB);
        }

        private void RemoveCurrentDeclarationsFromCopyListings()
        {
            _DeclarationsACopy.Remove(_CurrentDeclarationA);
            _DeclarationsBCopy.Remove(_CurrentDeclarationB);
        }

        private bool AllItemsInCopyListingsAreRemoved()
        {
            return _DeclarationsACopy.Count == 0 && _DeclarationsBCopy.Count == 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class SortedDeclarationCreator
    {
        private readonly IList<IDeclaration> _Declarations;
        private readonly IList<IDeclaration> _SortedDeclarations;

        public static ICollection<SortedDeclaration> CreateUnsorted(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            var extractor = new ClassDeclarationExtractor(classDeclaration);
            var declarations = extractor.GetDeclarations();
            return CreateUnsorted(declarations);
        }

        public static ICollection<SortedDeclaration> CreateUnsorted( IList<IDeclaration> declarations)
        {
            if (declarations == null) throw new ArgumentNullException("declarations");
            return Create(declarations, declarations);
        }

        public static ICollection<SortedDeclaration> Create(IList<IDeclaration> declarations, IList<IDeclaration> sortedDeclarations)
        {
            if (declarations == null) throw new ArgumentNullException("declarations");
            if (sortedDeclarations == null) throw new ArgumentNullException("sortedDeclarations");
            var creator = new SortedDeclarationCreator(declarations, sortedDeclarations);
            return creator.CreateSortedDeclarations();
        }

        private SortedDeclarationCreator(IList<IDeclaration> declarations, IList<IDeclaration> sortedDeclarations)
        {
            _Declarations = declarations;
            _SortedDeclarations = sortedDeclarations;
        }

        public ICollection<SortedDeclaration> CreateSortedDeclarations()
        {
            var sortedDeclarations = new Collection<SortedDeclaration>();
            foreach (var declaration in _SortedDeclarations)
            {
                var sortedDeclaration = CreateSortedDeclaration(declaration);
                sortedDeclarations.Add(sortedDeclaration);
            }
            return sortedDeclarations;
        }

        private SortedDeclaration CreateSortedDeclaration(IDeclaration declaration)
        {
            var oldIndex = GetOldIndex(declaration);
            var newIndex = GetNewIndex(declaration);
            return new SortedDeclaration(declaration, oldIndex, newIndex);
        }

        private int GetOldIndex(IDeclaration declaration)
        {
            return _Declarations.IndexOf(declaration);
        }

        private int GetNewIndex(IDeclaration declaration)
        {
            return _SortedDeclarations.IndexOf(declaration);
        }

    }
}

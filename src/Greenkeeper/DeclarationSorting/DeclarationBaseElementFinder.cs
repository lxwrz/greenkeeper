﻿using JetBrains.Application.Progress;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting 
{
    public class DeclarationBaseElementFinder
    {
        private IPsiServices _PsiServices;
        private readonly ICollection<IDeclaredElement> _DeclaredElements;

        public static DeclarationBaseElementFinder CreateFinder(ICollection<IDeclaration> declarations)
        {
            if (declarations == null) throw new ArgumentNullException("declarations");
            return new DeclarationBaseElementFinder(declarations.Select(d => d.DeclaredElement).ToList()); 
}       

        public DeclarationBaseElementFinder(ICollection<IDeclaredElement> declaredElements)
        {
            if (declaredElements == null) throw new ArgumentNullException("declaredElements");
            _DeclaredElements = declaredElements;
        }

        public ICollection<DeclarationBaseElement> Find()
        {
            if (_DeclaredElements.Count > 0)
            {
                SetPsiServices();
                return GetAllBaseElements();
            }
            return new Collection<DeclarationBaseElement>();
        }

        private void SetPsiServices()
        {
            _PsiServices = _DeclaredElements.First().GetPsiServices();
        }
        
        private ICollection<DeclarationBaseElement> GetAllBaseElements()
        {
            var baseElements = new List<DeclarationBaseElement>();
            foreach (var declaredElement in _DeclaredElements)
            {
                var elements = GetBaseElementsForDeclaredElement(declaredElement);
                baseElements.AddRange(elements);
            }
            return baseElements;
        }

        private ICollection<DeclarationBaseElement> GetBaseElementsForDeclaredElement(IDeclaredElement declaredElement)
        {
            var foundBaseElements = FindBaseElements(declaredElement);
            return CreateDeclarationBaseElements(declaredElement,foundBaseElements);
        }

        private IEnumerable<IDeclaredElement> FindBaseElements(IDeclaredElement declaredElement)
        {
            return _PsiServices.Finder.FindImmediateBaseElements(declaredElement, NullProgressIndicator.Instance);
        }

        private ICollection<DeclarationBaseElement> CreateDeclarationBaseElements(IDeclaredElement declaredElement, IEnumerable<IDeclaredElement> foundDeclaredElements)
        {
            var createdBaseElements = new Collection<DeclarationBaseElement>();
            foreach (var foundDeclaredElement in foundDeclaredElements)
            {
                var createdBaseElement = CreateDeclarationBaseElement(declaredElement, foundDeclaredElement);
                createdBaseElements.Add(createdBaseElement);
            }
            return createdBaseElements;
        }

        private DeclarationBaseElement CreateDeclarationBaseElement(IDeclaredElement declaredElement, IDeclaredElement baseDeclaredElement)
        {
            return new DeclarationBaseElement(declaredElement, baseDeclaredElement);
        }
    }
}

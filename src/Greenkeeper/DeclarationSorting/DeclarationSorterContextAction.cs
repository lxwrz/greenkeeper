﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.Application.Settings;
using JetBrains.DocumentModel;
using JetBrains.ReSharper.Feature.Services.Bulbs;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Intentions.Extensibility.Menu;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using JetBrains.UI.Icons;
using JetBrains.UI.Icons.CompiledIcons;
using NLog;
using System;

namespace Greenkeeper.DeclarationSorting
{
    [ContextAction(Group = "C#", Name = DeclarationSorterBulbAction.DeclarationSorterName, Description = DeclarationSorterBulbAction.DeclarationSorterDescription)]
    public class DeclarationSorterContextAction : IContextAction
    {
        private readonly Logger _Logger;
        private readonly ICSharpContextActionDataProvider _Provider;
        private readonly IContextBoundSettingsStore _Settings;
        private IDeclarationSorter _DeclarationSorter;
        private IAdditionalExecutor _AdditionalExecutor;

        public DeclarationSorterContextAction(ICSharpContextActionDataProvider provider)
        {
            if (provider == null) throw new ArgumentNullException("provider");
            _Provider = provider;
            _Settings = _Provider.PsiServices.SettingsStore.BindToContextTransient(ContextRange.ApplicationWide);
            _Logger = LogManager.GetCurrentClassLogger();
        }

        internal static DeclarationSorterContextAction Create(ICSharpContextActionDataProvider provider, IDeclarationSorter declarationSorter)
        {
            return Create(provider, declarationSorter, new NoAdditionalExecutor());
        }

        internal static DeclarationSorterContextAction Create(ICSharpContextActionDataProvider provider, IDeclarationSorter declarationSorter, IAdditionalExecutor additionalExecutor)
        {
            if (provider == null) throw new ArgumentNullException("provider");
            if (declarationSorter == null) throw new ArgumentNullException("declarationSorter");
            if (additionalExecutor == null) throw new ArgumentNullException("additionalExecutor");

            var contextAction = new DeclarationSorterContextAction(provider)
                {
                    _DeclarationSorter = declarationSorter,
                    _AdditionalExecutor = additionalExecutor
                };
            return contextAction;
        }
        
        private IBulbAction GetBulbAction()
        {
            return new DeclarationSorterBulbAction(GetSelectedClass(), _DeclarationSorter, _AdditionalExecutor);
        }

        private string GetContextActionLogDescription()
        {
            var selectedClass = GetSelectedClass();
            var className = string.Empty;
            if (selectedClass != null)
            {
                className = selectedClass.DeclaredName;
            }

            return "SortContextActionIsAvailable on " + className;
        }

        public IEnumerable<IntentionAction> CreateBulbItems()
        {
            return GetBulbAction().ToContextAction();
        }

        public bool IsAvailable(JetBrains.Util.IUserDataHolder cache)
        {
            return ExceptionLogger.Execute(_Logger, GetContextActionLogDescription, () => IsSortAvailableAndLogTrackedTime());
        }

        private bool IsSortAvailableAndLogTrackedTime()
        {
            return TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetContextActionLogDescription, () => IsSortAvailable());
        }

        private bool IsSortAvailable()
        {
            var selectedClass = GetSelectedClass();
            return IsCaretAtClassHeader() && selectedClass != null && IsClassNotSorted(selectedClass);
        }

        private bool IsClassNotSorted(IClassDeclaration classDeclaration)
        {
            SetSorterAndExecutor();
            _DeclarationSorter.InitWithClassDeclaration(classDeclaration);
            return !_DeclarationSorter.IsClassSorted();
        }

        private void SetSorterAndExecutor()
        {
            if (_AdditionalExecutor == null || _DeclarationSorter == null)
            {
                var definition = LoadDeclarationSortingDefinition();
                SetAdditionalExecutorIfNotAlreadySet(definition);
                SetDeclarationSorterIfNotAlreadySet(definition);
            }
        }

        private DeclarationSortingDefinition LoadDeclarationSortingDefinition()
        {
            var loader = new DeclarationSortingDefinitionFromSettingsLoader(_Settings);
            return loader.Load();
        }

        private void SetAdditionalExecutorIfNotAlreadySet(DeclarationSortingDefinition definition)
        {
            if (_AdditionalExecutor == null)
            {
                _AdditionalExecutor = definition.AdditionalExecutor;
            }
        }

        private void SetDeclarationSorterIfNotAlreadySet(DeclarationSortingDefinition definition)
        {
            if (_DeclarationSorter == null)
            {
                _DeclarationSorter = definition.DeclarationSorter;
            }
        }

        private IClassDeclaration GetSelectedClass()
        {
            return _Provider.GetSelectedElement<IClassDeclaration>(false, true);
        }

        private bool IsCaretAtClassHeader()
        {
            var classDeclaration = _Provider.GetSelectedElement<IClassDeclaration>(false, true);
            var node = _Provider.GetSelectedElement<ITreeNode>(false, true);
            return node != null && classDeclaration != null && ClassHeaderContainsTreeNode(classDeclaration, node);
        }

        private bool ClassHeaderContainsTreeNode(IClassDeclaration classDeclaration, ITreeNode treeNode)
        {
            var headerRange = GetHeaderRange(classDeclaration);
            var currentNode = treeNode.GetDocumentRange();
            return headerRange.Contains(currentNode);
        }

        private DocumentRange GetHeaderRange(IClassDeclaration classDeclaration)
        {
            var rangeDetector = new ClassDeclarationRangeDetector(classDeclaration);
            return rangeDetector.GetHeaderRange();
        }
    }
}
﻿using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Resolve;

namespace Greenkeeper
{
    public class DeclarationUsageFindResult
    {
        public IDeclaredElement DeclaredElement { get; private set; }
        public IReference Reference { get; private set; }

        public DeclarationUsageFindResult(IDeclaredElement declaredElement, IReference reference)
        {
            DeclaredElement = declaredElement;
            Reference = reference;
        }
    }
}
﻿using System;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterCanBeNullAttributeCriterion : INullCheckRequiredCriterion
    {
        private const string _CanBeNullAttributeName = "CanBeNull";

        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");


            var regularParameter = parameterDeclaration as IRegularParameterDeclaration;
            if (regularParameter == null)
            {
                return true;
            }

            return !ParameterContainsCanBeNullAttribute(regularParameter);
        }

        private static bool ParameterContainsCanBeNullAttribute(IRegularParameterDeclaration regularParameter)
        {
            var extractor = new AttributesExtractor(regularParameter);
            var attributes = extractor.GetAttributes();

            return attributes.Any(a => _CanBeNullAttributeName.Equals(a));
        }
    }
}

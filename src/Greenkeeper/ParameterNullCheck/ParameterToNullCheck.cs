﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterToNullCheck
    {
        public bool ParameterNeedNullCheck { get; private set; }
        public ICSharpStatement Statement { get; private set; }
        
        public bool ParameterHasNullCheck
        {
            get { return  Statement != null; }
        }

        public bool ParameterHasNotNeededNullCheck
        {
            get { return ParameterNeedNullCheck && !ParameterHasNullCheck; }
        }
        
        public IParameterDeclaration ParameterDeclaration { get; private set; }

        public ParameterToNullCheck(bool parameterNeedNullCheck, IParameterDeclaration parameterDeclaration, ICSharpStatement statement = null)
        {
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");

            ParameterDeclaration = parameterDeclaration;
            Statement = statement;
            ParameterNeedNullCheck = parameterNeedNullCheck;
        }
    }
}

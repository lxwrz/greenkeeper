﻿using JetBrains.Application.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.ParameterNullCheck
{
    [SettingsKey(typeof(EnvironmentSettings), "ParameterNullCheckSettings")]
    public class ParameterNullCheckSettings
    {
        public const string DefaultNullCheckStatement = "if ({0} == null) throw new ArgumentNullException(\"{0}\");"; 
        private static readonly char[] Separators = {',',';'};

        [SettingsEntry(true, "AddNotNullAttributeToParameter")]
        public bool AddNotNullAttributeToParameter { get; set; }

        [SettingsEntry("Test,TestCase,TestCaseSource", "FunctionAttributesThatIndicatesNoNullCheck")]
        public string FunctionAttributesThatIndicatesNoNullCheck { get; set; }

        [SettingsEntry(DefaultNullCheckStatement, "NullCheckStatement")]
        public string NullCheckStatement { get; set; }

        
        public ICollection<string> GetFunctionAttributesThatIndicatesNoNullCheck()
        {
            if (string.IsNullOrEmpty(FunctionAttributesThatIndicatesNoNullCheck))
            {
                return new Collection<string>();
            }

            return FunctionAttributesThatIndicatesNoNullCheck.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
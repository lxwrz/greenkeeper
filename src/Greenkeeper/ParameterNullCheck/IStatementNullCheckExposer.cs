﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using System.Collections.Generic;

namespace Greenkeeper.ParameterNullCheck
{
    public interface IStatementNullCheckExposer
    {
        ICollection<ICSharpStatement> GetNullChecksForParameter(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration, IExposerContext exposerContext);
        ICollection<ICSharpStatement> GetNullCheckForParameterConsiderOnlyCondition(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration, IExposerContext exposerContext);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckInFileRequiredDetector
    {
        private readonly IFile _File;
        private readonly ParameterNullCheckSettings _Settings;

        public ParameterNullCheckInFileRequiredDetector(IFile file,ParameterNullCheckSettings settings)
        {
            if (file == null) throw new ArgumentNullException("file");
            if (settings == null) throw new ArgumentNullException("settings");
            _File = file;
            _Settings = settings;
        }

        public ICollection<ICSharpFunctionWithParameterProvider> GetFunctionsNeedNullCheck()
        {
            var functionsNeedNullCheck = new Collection<ICSharpFunctionWithParameterProvider>();
            var functionWithParamters = CSharpFunctionWithParameterFactory.GetElements(_File);
            foreach (var functionWithParamter in functionWithParamters)
            {
                if (NeedNullCheck(functionWithParamter))
                {
                    functionsNeedNullCheck.Add(functionWithParamter);
                }
            }
            return functionsNeedNullCheck;
        }
            
        private bool NeedNullCheck(ICSharpFunctionWithParameterProvider functionDeclaration)
        {
            var detector = new ParameterNullCheckInFunctionRequiredDetector(functionDeclaration,_Settings);
            return detector.AnyParameterNeedNullCheck();
        }
    }
}

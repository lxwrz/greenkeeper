﻿using JetBrains.Annotations;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.ParameterNullCheck
{
    public abstract class StatementNullCheckExposer : IStatementNullCheckExposer
    {
        private ICSharpFunctionDeclaration _FunctionDeclaration;
        private ICSharpParameterDeclaration _ParameterDeclaration;

        public ICollection<ICSharpStatement> GetNullChecksForParameter(
            [NotNull] ICSharpFunctionDeclaration functionDeclaration,
            [NotNull] ICSharpParameterDeclaration parameterDeclaration, 
            [NotNull] IExposerContext exposerContext)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (exposerContext == null) throw new ArgumentNullException("exposerContext");
            _FunctionDeclaration = functionDeclaration;
            _ParameterDeclaration = parameterDeclaration;
            return FindParameterNullCheckStatement(IsNullCheckForParameter);
        }

        private ICollection<ICSharpStatement> FindParameterNullCheckStatement(
            Func<ICSharpStatement, string, bool> isNullCheckCondition)
        {
            var nullCheckStatements = new Collection<ICSharpStatement>();
            var methodBody = _FunctionDeclaration.Body;
            var parameterName = _ParameterDeclaration.DeclaredName;
            if (methodBody == null)
            {
                return nullCheckStatements;
            }

            foreach (var cSharpStatement in methodBody.Statements)
            {
                if (isNullCheckCondition(cSharpStatement, parameterName))
                {
                    nullCheckStatements.Add(cSharpStatement);
                }
            }
            return nullCheckStatements;
        }

        protected abstract bool IsNullCheckForParameter(ICSharpStatement cSharpStatement, string parameterName);

        public ICollection<ICSharpStatement> GetNullCheckForParameterConsiderOnlyCondition(
            [NotNull] ICSharpFunctionDeclaration functionDeclaration,
            [NotNull] ICSharpParameterDeclaration parameterDeclaration, 
            [NotNull] IExposerContext exposerContext)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (exposerContext == null) throw new ArgumentNullException("exposerContext");
            _FunctionDeclaration = functionDeclaration;
            _ParameterDeclaration = parameterDeclaration;
            return FindParameterNullCheckStatement(IsNullCheckForParameterConsiderOnlyCondition);
        }

        protected abstract bool IsNullCheckForParameterConsiderOnlyCondition(ICSharpStatement cSharpStatement,
                                                                             string parameterName);
    }
}

﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Linq;

namespace Greenkeeper.ParameterNullCheck
{
    public class MethodInvocationWithOneArgumentExposer : StatementNullCheckExposer
    {
        private const string _StringIsNullOrEmptyInvocation = "string.IsNullOrEmpty";
        private const string _StringIsNullOrWhiteSpaceInvocation = "string.IsNullOrWhiteSpace";
        private readonly string _MethodInvocation;
        private string _CheckedParameter;
        private IfStatementThrowArgumentNullExceptionData _IfStatement;
        private InvocationData _Invocation;

        private MethodInvocationWithOneArgumentExposer(string methodInvocation)
        {
            if (methodInvocation == null) throw new ArgumentNullException("methodInvocation");
            _MethodInvocation = methodInvocation;
        }

        protected override bool IsNullCheckForParameter(ICSharpStatement cSharpStatement, string parameterName)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            if (parameterName == null) throw new ArgumentNullException("parameterName");

            return IsNullCheckForParameterConsiderOnlyCondition(cSharpStatement, parameterName)
                   && IsThrowedParameterName(parameterName);
        }

        protected override bool IsNullCheckForParameterConsiderOnlyCondition(ICSharpStatement cSharpStatement, string parameterName)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            if (parameterName == null) throw new ArgumentNullException("parameterName");

            return IsNullCheckStatement(cSharpStatement) &&
                   IsParameterName(parameterName);
        }

        private bool IsNullCheckStatement(ICSharpStatement cSharpStatement)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            SetStatementParts(cSharpStatement);
            return _IfStatement.IsValid && IsValid();
        }

        private void SetStatementParts(ICSharpStatement cSharpStatement)
        {
            ExposeIfThrowStatement(cSharpStatement);
            ExposeInvocation();

            if (IsInvocationWithName())
            {
                SetCheckedParameter();
            }
        }

        private void ExposeIfThrowStatement(ICSharpStatement cSharpStatement)
        {
            var ifThrowExposer = new IfStatementThrowArgumentNullExceptionExposer();
            _IfStatement = ifThrowExposer.Expose(cSharpStatement);
        }

        private void ExposeInvocation()
        {
            if (_IfStatement.Condition != null)
            {
                var invocationExposer = new InvocationExposer();
                _Invocation = invocationExposer.Expose(_IfStatement.Condition);   
            }
        }

        private bool IsInvocationWithName()
        {
            return _Invocation != null && _Invocation.IsValid &&
                   _MethodInvocation.Equals(_Invocation.InvocationText, StringComparison.InvariantCultureIgnoreCase);
        }

        private void SetCheckedParameter()
        {
            if (_Invocation.Arguments.Count == 1)
            {
                _CheckedParameter = _Invocation.Arguments.First();
            }
        }

        private bool IsValid()
        {
            return _Invocation != null && _CheckedParameter != null;
        }

        private bool IsParameterName(string parameterName)
        {
            return String.Equals(_CheckedParameter, parameterName);
        }

        private bool IsThrowedParameterName(string parameterName)
        {
            return _IfStatement.ThrowedParameterName.Equals(parameterName);
        }

        public static MethodInvocationWithOneArgumentExposer CreateStringIsNullOrEmptyExposer()
        {
            return new MethodInvocationWithOneArgumentExposer(_StringIsNullOrEmptyInvocation);
        }

        public static MethodInvocationWithOneArgumentExposer CreateStringIsNullOrWhitespaceExposer()
        {
            return new MethodInvocationWithOneArgumentExposer(_StringIsNullOrWhiteSpaceInvocation);
        }
    }
}
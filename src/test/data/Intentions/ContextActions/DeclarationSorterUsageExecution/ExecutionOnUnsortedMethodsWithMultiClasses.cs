class {caret} A
{
  private void A()
  {
  
  }

  public void C()
  {
	A();
	D();
  }

  private void E()
  {
	A();
  }

  private void D()
  {
  
  }
}

class B
{

  public void T()
  {
  var a = new A();
  a.C();
  }

}
class {caret} A
{
	private const string _A = "A";
	private const string _B = "B";
	private const string _C = "C";
	private const string _D = _C + _B + _A;
}
class A
{
  [Test]
  public A(string arg1{off})
  {	
  }  
  
  [Test]
  public A(string arg1,string arg2{off})
  {
  } 
  
  [Test]
  public A(string arg1,string arg2,string arg3{off})
  {
  } 

  [Test]
  public void A(string arg1{off})
  {	
  }  
  
  [Test]
  public void A(string arg1,string arg2{off})
  {	
  } 
  
  [Test]
  public void A(string arg1,string arg2,string arg3{off})
  {  
  } 
}
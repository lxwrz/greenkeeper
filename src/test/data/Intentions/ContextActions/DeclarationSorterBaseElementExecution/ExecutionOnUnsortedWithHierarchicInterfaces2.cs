namespace Test
{	
	interface IA : ID
	{
	  void A();	  
	}
	
	interface IC
	{	   
	  void E();
	}
	
	interface IB : IC
	{
	  void D(); 	  
	}	
	
	interface ID : IE
	{	   
	  void B();	  
	}
	
	interface IE
	{	   
	  void C();
	}	
	
	class {caret} A : IB, IA 
	{
	  public void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	}
}
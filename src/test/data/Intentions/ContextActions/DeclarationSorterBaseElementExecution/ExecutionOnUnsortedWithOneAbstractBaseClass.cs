namespace Test
{	
	abstract class IA
	{
	  public abstract void A();
	  public abstract void B();
	  public abstract void C();
	  public abstract void D();
	  public abstract void E();	  
	}

	class {caret} A : IA
	{
	  public override void C()
	  {  
	  }

	  public override void E()
	  {  
	  }

	  public override void D()
	  {  
	  }
	  
	  public override void A()
	  {  
	  }
	  
	  public override void B()
	  {  
	  }  
	}
}
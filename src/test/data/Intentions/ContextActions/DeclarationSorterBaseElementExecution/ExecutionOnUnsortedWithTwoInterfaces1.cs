namespace Test
{	
	interface IA
	{
	  void A();  
	  void B();
	  void C();
	}
	
	interface IB
	{
	  void D();
	  void E();
	}	
	
	class {caret} A : IA, IB
	{
	  public void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	}
}
namespace Test
{	
	class IA
	{
	  public virtual void A()
	  {
	  }
	  
	  public virtual void B()
	  {
	  }
	  
	  public virtual void C()
	  {
	  }
	  
	  public virtual void D()
	  {
	  }
	  
	  public virtual void E()
	  {
	  }
	}

	class {caret} A : IA
	{
	  public override void C()
	  {  
	  }

	  public override void E()
	  {  
	  }

	  public override void D()
	  {  
	  }
	  
	  public override void A()
	  {  
	  }
	  
	  public override void B()
	  {  
	  }  
	}
}
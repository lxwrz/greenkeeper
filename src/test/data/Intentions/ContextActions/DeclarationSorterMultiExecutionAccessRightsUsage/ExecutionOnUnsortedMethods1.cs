class {caret} A
{
  private void A()
  {
  
  }

  private void C()
  {
	A();
	D();
  }

  public void E()
  {
	A();
  }

  private void D()
  {
  
  }
}
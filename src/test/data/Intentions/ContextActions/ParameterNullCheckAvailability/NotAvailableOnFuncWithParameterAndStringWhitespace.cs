class A
{
  public A(string arg1{off})
  {
	if (String.IsNullOrWhiteSpace(arg1)) throw new ArgumentNullException("arg1");
  }  
  
  public A(string arg1,string arg2{off})
  {
	if (String.IsNullOrWhiteSpace(arg1)) throw new ArgumentNullException("arg1");
	if (String.IsNullOrWhiteSpace(arg2)) throw new ArgumentNullException("arg2");
  } 
  
  public A(string arg1,string arg2,string arg3{off})
  {
    if (string.IsNullOrWhiteSpace(arg2)) throw new ArgumentNullException("arg2");
	if (String.IsNullOrWhiteSpace(arg1)) throw new ArgumentNullException("arg1");
	if (String.IsNullOrWhiteSpace(arg3)) throw new ArgumentNullException("arg3");
  } 

  public void A(string arg1{off})
  {
	if (String.IsNullOrWhiteSpace(arg1)) throw new ArgumentNullException("arg1");
  }  
  
  public void A(string arg1,string arg2{off})
  {
	if (String.IsNullOrWhiteSpace(arg1)) throw new ArgumentNullException("arg1");
	if (String.IsNullOrWhiteSpace(arg2)) throw new ArgumentNullException("arg2");
  } 
  
  public void A(string arg1,string arg2,string arg3{off})
  {
    if (String.IsNullOrWhiteSpace(arg2)) throw new ArgumentNullException("arg2");
	if (String.IsNullOrWhiteSpace(arg1)) throw new ArgumentNullException("arg1");
	if (String.IsNullOrWhiteSpace(arg3)) throw new ArgumentNullException("arg3");
  } 
}
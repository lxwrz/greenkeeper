class A
{
  public A([CanBeNull]string arg1{off})
  {
  } 

  public A([CanBeNull]string arg1,string arg2{on})
  {	
  }
  
  public A([CanBeNull]string arg1,[CanBeNull]string arg2{off})
  {	
  }
  
  public A([CanBeNull]string arg1,string arg2{off})
  {
	if (arg2 == null) throw new ArgumentNullException("arg2");
  }
  
  public void B([CanBeNull]string arg1{off})
  {
  }
  
  public void B([CanBeNull]string arg1,string arg2{on})
  {
  }
  
  public void B([CanBeNull]string arg1,[CanBeNull]string arg2{off})
  {
  }
  
  public void B([CanBeNull]string arg1,string arg2{off})
  {
	if (arg2 == null) throw new ArgumentNullException("arg2");
  }
}

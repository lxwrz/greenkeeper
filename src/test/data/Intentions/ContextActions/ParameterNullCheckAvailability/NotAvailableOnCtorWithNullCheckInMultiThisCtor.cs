class A
{
  public A(string arg1{off})
	: this("",arg1)
  {    
  }
  
  public A(string a1, string a2)
	: this("","",a2)
  {    	
  } 
  
  public A(string a1, string a2, string a3)
	: this("","","",a3)
  {    	
  } 
  
  public A(string a1, string a2, string a3, string a4)
  {    	
	if (a4 == null) throw new ArgumentNullException("a4");
  }
}
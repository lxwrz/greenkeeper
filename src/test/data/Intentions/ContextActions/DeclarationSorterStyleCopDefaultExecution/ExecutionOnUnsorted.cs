namespace Test
{
    public class {caret} TestClass : ABaseClass, AInterface
    {
        public override void FMethod()
        {
        }


        protected static void MethodL()
        {
        }

        private int _S;
        private int PropE { get; set; }

        public int PropF { get; set; }

        internal override void DMethod()
        {
        }
        private int this[int a, int b]
        {
            get
            {
                return arr[a];
            }
            set
            {
                arr[a] = value;
            }
        }

        private static event TestDelegateA SampleEventJ;

        private static int PropK { get; set; }

        protected int _Q;
        private int PropB { get; set; }

        internal static event TestDelegateA SampleEventE;


        protected delegate void TestDelegateG(string test);
        internal int PropC { get; set; }

        protected static event TestDelegateA SampleEventI;

        protected TestClass(int a, int b, int c)
        {

        }

        public int PropA { get; set; }

        private event TestDelegateA SampleEventL;

        private delegate void TestDelegateB(string test);
        private event TestDelegateA SampleEventK;

        internal TestClass(int a, int b, int c, int d)
        {
        }

        public int _R;

        internal void MethodI()
        {
        }

        protected static int PropH { get; set; }

        protected static int _J;
        private static int PropL { get; set; }

        protected int PropD { get; set; }

        private readonly int _E;

        private TestClass(int a, int b, int c, int d, int e)
        {

        }

        public event TestDelegateA SampleEventA;


        ~TestClass()
        {

        }

        internal int this[int a, int b, int c, int d, int e]
        {
            get
            {
                return arr[a];
            }
            set
            {
                arr[a] = value;
            }
        }

        private event TestDelegateA SampleEventB;

        public static void MethodD()
        {
        }

        protected delegate void TestDelegateD(string test);


        protected const int _C = 0;
        public static int PropI { get; set; }

        public static event TestDelegateA SampleEventG;

        private const int _A = 0;
        protected event TestDelegateA SampleEventC;

        internal int this[int a, int b, int c, int d]
        {
            get
            {
                return arr[a];
            }
            set
            {
                arr[a] = value;
            }
        }


        public static void MethodB()
        {
        }
        public const int _B = 0;
        internal static int PropJ { get; set; }

        public delegate void TestDelegateA(string test);


        private TestClass(int a)
        {

        }
        protected static event TestDelegateA SampleEventH;

        public int _N;
        private static event TestDelegateA SampleEventF;

        private static int _I;

        private static void MethodG()
        {
        }

        internal readonly int _G;
        internal event TestDelegateA SampleEventD;
        public void MethodC()
        {
        }

        internal static int _L;
        internal int _O;

        internal TestClass(int a, int b)
        {


        }
        public delegate void TestDelegateE(string test);

        public static TestClass operator *(TestClass a, TestClass b)
        {
            return new TestClass();
        }
        public void AMethod()
        {

        }
        public void MethodA()
        {
        }

        protected int this[int a, int b, int c]
        {
            get
            {
                return arr[a];
            }
            set
            {
                arr[a] = value;
            }
        }

        protected readonly int _F;

        protected void MethodJ()
        {
        }

        public readonly int _H;


        private void MethodE()
        {
        }


        private int _P;

        public TestClass()
        {

        }
        protected delegate void TestDelegateF(string test);

        internal const int _D = 0;
        public static int _K;

        protected static void MethodK()
        {
        }
        static TestClass()
        {
        }

        protected override void GMethod()
        {
        }


        protected static int PropG { get; set; }

        protected int _M;

        internal static void MethodH()
        {
        }
        public void BMethod()
        {

        }

        internal delegate void TestDelegateC(string test);

        public static TestClass operator -(TestClass a, TestClass b)
        {
            return new TestClass();
        }

        private int[] arr;

        private static void MethodF()
        {
        } 
        

        public int this[int a]
        {
            get
            {
                return arr[a];
            }
            set
            {
                arr[a] = value;
            }
        }
        public void CMethod()
        {

        }

        protected override void HMethod()
        {
        }

        public static TestClass operator +(TestClass a, TestClass b)
        {
            return new TestClass();
        }
       
        internal override void EMethod()
        {
        }
    }

    public class ABaseClass
    {
        public virtual void FMethod()
        {
        }

        internal virtual void EMethod()
        {
        }

        internal virtual void DMethod()
        {
        }

        protected virtual void HMethod()
        {
        }

        protected virtual void GMethod()
        {
        }
    }

    public interface AInterface
    {
        void CMethod();
        void BMethod();
        void AMethod();
    }
}

﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Psi.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Greenkeeper.Tests.DeclarationSorting
{
    [TestFixture]
    public class SortedIndexDeclarationComparerTests
    {
        [TestCase(null, null, 0)]
        [TestCase(1, null, -1)]
        [TestCase(null, 1, 1)]
        [TestCase(1, 1, 0)]
        [TestCase(1, 2, -1)]
        [TestCase(2, 1, 1)]
        public void CompareTest(int? xSortedIndex, int? ySortedIndex, int result)
        {
            Assert.AreEqual(result, Compare(xSortedIndex, ySortedIndex));
        }

        private int Compare(int? xSortedIndex, int? ySortedIndex)
        {
            var mockedX = MockDeclarationWithCompareIndex(xSortedIndex);
            var mockedY = MockDeclarationWithCompareIndex(ySortedIndex);

            var indexComparer = new SortedIndexDeclarationComparer();
            return indexComparer.Compare(mockedX, mockedY);
        }

        private DeclarationOrderElement MockDeclarationWithCompareIndex(int? sortedIndex)
        {
            var mocked = MockRepository.GenerateMock<IDeclaration>();
            return new DeclarationOrderElement(mocked, null, sortedIndex);
        }
    }
}
